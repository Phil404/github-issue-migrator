# GitHub Issue Migrator

With this CLI-Tool you can transfer your Product Management Section from GitHub to GitLab.


## Commands
```bash
$> java -jar gitHubIssueMigrator.jar --version
GitHub Issue Migrator (Version: 0.0.1)

$> java -jar gitHubIssueMigrator.jar --help 
Welcome to the manual:
Currently work in progress!
```