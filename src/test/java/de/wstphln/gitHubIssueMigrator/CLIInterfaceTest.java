package de.wstphln.gitHubIssueMigrator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


class CLIInterfaceTest extends CLIInterface {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private final static String ARGUMENT_EXCEPTION_MESSAGE = "Invalid Parameters are set! Type '--help' for manual.";
    private final static String HELP_MANUAL = "Welcome to the manual:\nCurrently work in progress!";
    private final static String VERSION = "GitHub Issue Migrator (Version: 0.0.1)";

    @Test
    void whenNoParameterGiven_ThenThrowAnIllegalArgumentException() {
        String[] emptyParameter = {};
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> CLIInterface.main(emptyParameter));
        assertEquals(ARGUMENT_EXCEPTION_MESSAGE, exception.getMessage());
    }

    @Test
    void whenNonSenseParametersGiven_ThenThrowAnIllegalArgumentException() {
        String[] nonSenseParameter = {"Honolulu"};
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> CLIInterface.main(nonSenseParameter));
        assertEquals(ARGUMENT_EXCEPTION_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAskedForHelp_ThenReturnManual() {
        setUpConsoleStreams();
        String[] helpParameter = {"--help"};
        CLIInterface.main(helpParameter);
        assertEquals(HELP_MANUAL, outContent.toString());
        restoreStreams();
    }

    @Test
    void whenAskedForVersion_ThenReturnVersion() {
        setUpConsoleStreams();
        String[] versionParameter = { "--version" };
        CLIInterface.main(versionParameter);
        assertEquals(VERSION, outContent.toString());
        restoreStreams();
    }

    @Test
    void whenAskedForVersionWithCapitalLetters_ThenReturnVersion() {
        setUpConsoleStreams();
        String[] versionParameter = { "--VERSION" };
        CLIInterface.main(versionParameter);
        assertEquals(VERSION, outContent.toString());
        restoreStreams();
    }

    /**
     * This will set up the console catcher.
     * It will store all system.out.print calls.
     */
    private void setUpConsoleStreams() {
        System.setOut(new PrintStream(outContent));
    }

    /**
     * This will restore the console catcher.
     */
    private void restoreStreams() {
        System.setOut(originalOut);
    }

}
