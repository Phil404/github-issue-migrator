package de.wstphln.gitHubIssueMigrator;

public class CLIInterface {

	private final static String INVALID_ARGUMENT_MESSAGE = "Invalid Parameters are set! Type '--help' for manual.";
	private final static String HELP_MANUAL = "Welcome to the manual:\nCurrently work in progress!";
	private final static String VERSION = "GitHub Issue Migrator (Version: 0.0.1)";

	public static void main(String[] args) {
		if (args.length == 0) {
			throw new IllegalArgumentException(INVALID_ARGUMENT_MESSAGE);
		}
		switch (args[0].toLowerCase()) {
			case "--help":
				System.out.print(HELP_MANUAL);
				break;
			case "--version":
				System.out.print(VERSION);
				break;
			default:
				throw new IllegalArgumentException(INVALID_ARGUMENT_MESSAGE);
		}
	}

}
